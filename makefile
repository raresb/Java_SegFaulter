### 64 bit compiler required  
CC=gcc
### Replace with path to your to jdk include dir
JINCLUDE=C:/Program Files/Java/jdk1.8.0_111/include
###
JC=javac 
CLASS=Main.class SegFaulter.class 
JLIB=segfault.dll

app: $(CLASS) $(JLIB)

$(CLASS): %.class: %.java 
	$(JC) $^
	
$(JLIB): SegFaulter.c SegFaulter.h
	$(CC) -Wall -O -o $@ -s -shared -m64 $^ \
	-Iinclude \
	-I"$(JINCLUDE)" \
	-I"$(JINCLUDE)/win32" \
	-lm -Wl,--subsystem,windows 
	
SegFaulter.h: SegFaulter.class 
	javah -jni SegFaulter
	
.PHONY: clean

clean:
	rm -f *.exe *.out *.dll *.class

